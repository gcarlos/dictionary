# Dictionary project

The project seemed to be simple at first, but there are/were many details that need/needed to be done 
on the development.

I had the idea that the content returned from the dictionary will contain just a definition, 
and then I will show it, I will have to save just a few words with its definitions. But the 
content returned was very complex, a word can contains many definitions, senses, subsenses, 
categories, examples, pronounciations, and so on. 
I had to decide what to show on the application. I tried to show some useful information. 
That affected my entities on the DB, and the way to show the information.

Another thing was what to use to retrieve the content, I did not used any library for this
purpose before, so I did a little research on this, and I saw gem HttParty facilitated a lot 
on that task.

One of the things I had questions about was how to separate the business logic from the 
controller. I had some discussions with Eduardo on this. On Java I used MVC frameworks, and they
have a special layer between controller and model for the business logic. Usually the model 
layer is composed of the service, dao (Data Access Objects) and object pojos. And 
I did not saw that structure on Rails. We only have View, Controllers and Models as ActiveRecords.
So I had some questions how to organize the business logic.
Eduardo told me that the business logic should be in the model objects mostly, and on private 
methods in the controller.
I did something similar to that. I put some logic on the controller that directly works with 
the view, and the rest I put in an empty class localted on the model.
I think I need to do some research on this part to understant better how the ROR framework organize
that kind of logic.

Finally, I could mention also some of the difficulties I had on using the different functions 
available for arrays. The part of processing the result returned fromt the API was very complex.
I could not exploit much of the functions available for list and json, I am not much familiar 
with them, and I did some manual iterations on the result. Probably I could use functions like 
merge, folds, regular expressions, or things like that.

It was a great experience developing the project, simple and challenging. 