Rails.application.routes.draw do
  root to: 'words#home'

  get '/:wd' => 'words#home'
  post 'words/search'
end
