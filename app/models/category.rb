class Category < ApplicationRecord
  belongs_to :word
  has_many :definitions
end
