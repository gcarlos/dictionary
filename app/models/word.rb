class Word < ApplicationRecord
  has_many :categories
  validates :txt, presence: true, allow_blank: false
end
