class Search
  include HTTParty

  base_uri Rails.application.secrets.dictionary_base_uri
  headers 'app_id' => Rails.application.secrets.dictionary_app_id
  headers 'app_key' => Rails.application.secrets.dictionary_app_key

  def search(text)
    response = self.class.get("/api/v1/entries/en/#{text}", {})

    if response.success?
      txt = response['results'][0]['word']
      word_obj = Word.new
      word_obj.txt = txt

      categories = Array.new

      response['results'][0]['lexicalEntries'].each do |lex_entry|

        definitions = Array.new

        lex_entry['entries'].each do |entry|
          entry['senses'].each do |sense|

            examples = Array.new

            if ! sense['examples'].blank?
              sense['examples'].each do |example|
                example_value = example['text']

                example_obj = Example.new
                example_obj.example = example_value
                examples.push(example_obj)
              end
            end

            if ! sense['definitions'].blank?
              definition_value = sense['definitions'].join("; ")

              definition_obj = Definition.new
              definition_obj.definition = definition_value
              definition_obj.examples = examples

              definitions.push(definition_obj)
            end
          end
        end

        category_value = lex_entry['lexicalCategory']
        category_obj = Category.new
        category_obj.category = category_value
        category_obj.definitions = definitions

        categories.push(category_obj)
      end

      word_obj.categories = categories

      word_obj
    else
      case response.code
        when 404
          error_message = "No information available for '#{text}'."
        when 400
          error_message = "The request was invalid or cannot be otherwise served."
        when 403
          error_message = "The request failed due to invalid credentials. Please contact the administrator."
        else
          error_message = "Something is broken. Please contact us to fix the error."
      end

      raise error_message
    end
  end
end