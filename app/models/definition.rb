class Definition < ApplicationRecord
  belongs_to :category
  has_many :examples
end
