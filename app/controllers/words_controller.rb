class WordsController < ApplicationController
  def home
    if params[:wd].blank?
      @word = Word.new
    else
      if Word.exists?(:txt => params[:wd])
        @word = Word.find_by_txt(params[:wd])
      else
        if ! search_and_save(params[:wd])
          respond_to do |format|
            format.html { redirect_to root_path}
          end
        end
      end
    end
  end

  def search
    @word = Word.new(word_params)

    if @word.valid?
      if Word.exists?(:txt => @word.txt)
        respond_to do |format|
          format.html { redirect_to :action => "home", :wd => @word.txt}
        end
      else
        if search_and_save(@word.txt)
          respond_to do |format|
            format.html { redirect_to :action => "home", :wd => @word.txt}
          end
        else
          respond_to do |format|
            format.html { redirect_to root_path}
          end
        end
      end
    else
      respond_to do |format|
        format.html {render :home}
      end
    end
  end

  private
    def word_params
      params.require(:word).permit(:txt)
    end

    def search_and_save(text)
      begin
        search = Search.new
        @word = search.search(text)
        save_word(@word)
        return true
      rescue => error
        flash[:notice] = error.message
        return false
      end
    end

    def save_word(word)
      if Word.all.length >= 5
        Word.first.delete
      end

      word.save
    end
end
