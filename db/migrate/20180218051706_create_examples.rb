class CreateExamples < ActiveRecord::Migration[5.1]
  def change
    create_table :examples do |t|
      t.text :example
      t.references :definition, foreign_key: true

      t.timestamps
    end
  end
end
